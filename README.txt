CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration
  * Configuration Options


INTRODUCTION
------------

Current Maintainer: Justin Furnas <jfurnas@cenmi.org>

This module was created for Center for Educational Networkings developers and 
content editors to easily identify staging sites from production sites during
content editing and site maintenance and feature requests. It places a 
floating widget on your site. There are configuration options available for 
positioning the widget, as well as changing the widget text.

INSTALLATION
------------

This module installs the same as any basic Drupal contrib module. Once you 
extract the module into your modules folder, enable it in the 'Modules' 
page.

CONFIGURATION
-------------

Configuration of this module is simple. The configuration page can be
accessed at the following url: 'admin/config/system/staging-widget'. 

CONFIGURATION OPTIONS
---------------------

There are only a couple of configuration options available on this
widget;

  1.)  The 'Widget Text' field specifies the value that displays on 
       the widget.

  2.) The 'Top Alignment' field specifies how far from the top of 
      the page the widget should appear. This is a required field. 

  3.) The 'Left Alignment' field specifies how far off the left of 
      the page the widget should appear. 

  4.) The 'Right Alignment' field specifies how far off the right of 
      the page the widget should appear. 

NOTE: If you want the widget to appear on the right side of 
the screen, remove the value from 'Left Alignment' and set your 
pixel value on the 'Right Alignment' field.
